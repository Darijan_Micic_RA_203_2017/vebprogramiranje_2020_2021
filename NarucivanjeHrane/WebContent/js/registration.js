$(document).ready(function(){
	
	$('#registerForm').submit( (event)=>{
		event.preventDefault();
		
		var username = $('#username').val();
		var firstname = $('#firstName').val();
		var lastname = $('#lastName').val();
		var gender = $('#gender').val();
		var password = $('#password').val();

		if(username == "" || username == undefined || username == null){
			document.getElementById("alertUsername").innerHTML = "Niste uneli korisnicko ime";
		}else if(firstname == "" || firstname == undefined || firstname == null){
			document.getElementById("alertUsername").innerHTML = "";
			document.getElementById("alertFirstname").innerHTML = "Niste uneli Ime";
		}else if(lastname == "" || lastname == undefined || lastname == null){
			document.getElementById("alertUsername").innerHTML = "";
			document.getElementById("alertFirstname").innerHTML = "";
			document.getElementById("alertLastname").innerHTML = "Niste uneli prezime";
		}else if(gender == "" || gender == undefined || gender == null){
			document.getElementById("alertUsername").innerHTML = "";
			document.getElementById("alertFirstname").innerHTML = "";
			document.getElementById("alertLastname").innerHTML = "";
			document.getElementById("alertGender").innerHTML = "Niste uneli pol";
		}else if(password == "" || password == undefined || password == null){
			document.getElementById("alertUsername").innerHTML = "";
			document.getElementById("alertFirstname").innerHTML = "";
			document.getElementById("alertLastname").innerHTML = "";
			document.getElementById("alertGender").innerHTML = "";
			document.getElementById("alertPassword").innerHTML = "Niste uneli lozinku";
		}else {
			var obj = { "username":$('#username').val(), "firstName" : $('#firstName').val(),
					"lastName" : $('#lastName').val(), "gender" : $('#gender').val(),
					"password" : $('#password').val()};
			   
			console.log(JSON.stringify(obj));
			$.ajax({
		    	contentType: 'application/json',
		        url: '../NarucivanjeHrane/api/registration',
		        type : 'POST',
		        data: JSON.stringify(obj),
		        success: function(response){
		        	if(response==null){
	            		console.log('NULL');
	            		console.log(response);
	            		alert('Pogresno Korisnicko ime ili Lozinka.');
	            	}else{	            		
			        	alert('Uspesno ste se registrovali');
			        	window.location = './login.html';
			        }
		        }
		    });
		}
		
	});
})
