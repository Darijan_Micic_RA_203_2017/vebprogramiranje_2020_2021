$(document).ready(function(){

	$('#podaciDiv').html('');
	$('#podaci').click(prikaziPodatke());
	$('#porudzbine').click(prikazPorudzbina());
	$('#nedostavljene').click(prikazNedostavljenih());
	$('#komentari').click(prikazKomentara());
	$('#logout').click(logout());
})

function logout(){
	return function(event){
		event.preventDefault();
		
		$.ajax({
			url : '../NarucivanjeHrane/api/logout',
			type : 'GET',
			success : function(){
				alert('Uspijesno izlogovani!');
				window.location = './index.html';
			}
		});
	}
}

function prikaziPodatke(){
	
	return function(event){
		event.preventDefault();
				
		$.ajax({
			url: 'api/user',
			type: 'GET',
			contentType : 'application/json',
			success : function(user){
					console.log(user);
					ispisiPodatke(user);
				
			}
		});
	}
	
}


function ispisiPodatke(user){	
	let div = $('#podaciDiv');

	let div2 = $('<div height:2000px; margin-top: 300px;"></div>');
	let tabela = $('<p style="color:#FEE12B;"><b>Korisnicko ime:</b></p>' + user.username + '<br><br>');
	let tabela2 = $('<p style="color:#FEE12B;"><b>Ime:</b></p><input type="text" id="imeEdit" value="' + user.firstName + '" /><br><br>');
	let tabela3 = $('<p  style="color:#FEE12B;"><b>Prezime:</b></p><input type="text" id="lastEdit" value="' + user.lastName + '" /><br><br>');
	let tabela4 = $('<p  style="color:#FEE12B;"><b>Lozinka:</b></p><input type="text" id="lozinkaEdit" value="' + user.password + '" /><br><br>');
	let tabela5 = $('<p  style="color:#FEE12B;"><b>Pol:</b></p><input type="text" id="polEdit" value="' + user.gender + '" /><br><br>');
	
	let button = $('<button id="izmeniButton" style="background-color: #FEE12B;">Izmeni podatke</button>');
	
	button.on('click', function(event){
	
		let newIme = document.getElementById('imeEdit').value;
		let newPrezime = document.getElementById('lastEdit').value;
		let newLozinka = document.getElementById('lozinkaEdit').value;
		let newPol = document.getElementById('polEdit').value;	
		
		if(newIme == null || newIme == undefined || newIme == ""){
			alert('Ime ne smije biti prazno!');
		}else if(newPrezime == null || newPrezime == undefined || newPrezime == ""){
			alert('Prezime ne smije biti prazno!');
		}else if(newLozinka == null || newLozinka == undefined || newLozinka == ""){
			alert('Lozinka ne smije biti prazna!');
		}else if(newPol == null || newPol == undefined || newPol == ""){
			alert('Pol ne smije biti prazan!');
		}else{
			izmena(newIme, newPrezime, newLozinka, newPol);
		}
	});
	
	
	div2.append(tabela).append(tabela2).append(tabela3).append(tabela4).append(tabela5).append(button);
	div.append(div2);
	
}

function izmena(ime, prezime, lozinka, pol){
	var obj = {"firstName": ime, "lastName": prezime, "password": lozinka, "gender": pol};
	$('#podaciDiv').html('');
	$.ajax({
		url: 'api/user/edit',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(result){
			ispisiPodatke(result);
			alert('Uspesno ste izmenili podatke')
		},
		error : function(){
			alert('Došlo je do greške prilikom izmene podataka!');
		}
	});
}


function prikazPorudzbina(){
	return function(event){
		event.preventDefault();

		$('#porudzbineDIV').html('');		
		//$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/order/allOrdersB',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiPorudzbine(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza porudzbina!');
			}
		});
	}
}

function ispisiPorudzbine(porudzbina){
	let divR = $('#porudzbineDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiPodatkePorudzbineDIV(porudzbina);
	let komentar = ispisiKomentare(porudzbina);

	
	div.append(podaci).append(komentar);
	divR.append(div);
}

function ispisiKomentare(porudzbina){
	let button;
	
	if(porudzbina.status == "DELIVERED") {
		button = 	$('<input type="button" id="komentariDivButton' + porudzbina.id + '" style="margin-left: 20px; width: 90%;" value="Dodaj komentar" />');
		
		button.on('click', function(event){
			console.log('Kliknuta poridzbina: '+ porudzbina.id);			
			komentari();
		});
	}
	
	return button;
	
}

function komentari(){
	//var obj = {"id": porudzbina.id};
	
		$.ajax({
			type : 'GET',
			success : function(){
				window.location = './addComment.html';
			}
		});
	
}

function ispisiPodatkePorudzbineDIV(porudzbina){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let id = $('<h3> <i> Porudzbina : </i>' + porudzbina.id + '</h3>');
	let k = $('<h3> <i> Status porudzbine: </i>' + porudzbina.status  +'</h3>');
	let artikli = $('<h3> <i> Poručeni artikl: </i>' + porudzbina.articles.name + '</h3>');
	let kolicina =  $('<h3> <i> Količina: </i>' + porudzbina.articles.amount + '</h3>');
	let cena =  $('<h3> <i> Cena po komadu: </i>' + porudzbina.articles.price + '</h3>');
	
	
	podaci.append(id).append(k).append(artikli).append(kolicina).append(cena);
	return podaci;
}

function prikazNedostavljenih(){
	return function(event){
		event.preventDefault();

		$('#nedostavljeneDIV').html('');		
		$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/order/allOrdersBNedostavljene',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiNPorudzbine(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza porudzbina!');
			}
		});
	}
}

function ispisiNPorudzbine(porudzbina){
	let divR = $('#nedostavljeneDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let odustani  = ispisiOdustani(porudzbina);
	
	let podaci = ispisiNPodatkePorudzbineDIV(porudzbina);
	
	
	div.append(podaci).append(odustani);
	divR.append(div);
}

function ispisiOdustani(porudzbina){
	let button;
	if(porudzbina.status == "IN_PROCESS"){
		button = $('<button id="obradaPorudzbine' + porudzbina.id + '" style="margin-left: 20px; width: 90%;"> Odustani od porudzbine </button>');
		
		button.on('click', function(event){
			odustani(porudzbina);
		});
	}
	return button;
}

function odustani(porudzbina){
	var obj = {"id": porudzbina.id};
	
	$.ajax({
		url: 'api/order/status/odustao',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(response){
			alert('Odustali ste od porudzbine');
		},
		error : function(response){
			alert('Ne mozete odustati od porudzbine!');
		}
	});
}

function ispisiNPodatkePorudzbineDIV(porudzbina){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let id = $('<h3> <i> Porudzbina : </i>' + porudzbina.id + '</h3>');
	let k = $('<h3> <i> Status porudzbine: </i>' + porudzbina.status +'</h3>');
	let artikli = $('<h3> <i> Poručeni artikl: </i>' + porudzbina.articles.name + '</h3>');
	let kolicina =  $('<h3> <i> Količina: </i>' + porudzbina.articles.amount + '</h3>');
	let cena =  $('<h3> <i> Cena po komadu: </i>' + porudzbina.articles.price + '</h3>');
	
		
	podaci.append(id).append(k).append(artikli).append(kolicina).append(cena);
	return podaci;
}


function prikazKomentara(){
	return function(event){
		event.preventDefault();

		$('#komentariDIV').html('');		
		$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/comment/allAcceptB',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiKomentareAccept(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza komentara!');
			}
		});
	}
}

function ispisiKomentareAccept(komentar){
	let divR = $('#komentariDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	
	let podaci = ispisiAcceptKomentareDIV(komentar);
	
	
	div.append(podaci);
	divR.append(div);
}

function ispisiAcceptKomentareDIV(komentar){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let name = $('<h3> <i> Restoran : </i>' + komentar.restaurant.name + '</h3>');
	let text = $('<h3> <i> Komentar: </i>' + komentar.text +'</h3>');
	let grade = $('<h3> <i> Ocena: </i>' + komentar.grade + '</h3>');
	
	podaci.append(name).append(text).append(grade);
	return podaci;
}