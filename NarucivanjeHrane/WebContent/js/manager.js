$(document).ready(function(){

	$('#podaciDiv').html('');
	
	$('#restorani').click(prikaziRestorane());
	$('#porudzbine').click(prikaziPorudzbine());
	$('#dodajArtikl').click(kreirajArtikl());
	$('#sviArtikli').click(prikaziArtikl());
	$('#komentari').click(prikazKomentara());
	$('#izmena').click(izmeniSveArtikle());
	
	$('#podaci').click(prikaziPodatke());
	$('#logout').click(logout());
})

function logout(){
	return function(event){
		event.preventDefault();
		
		$.ajax({
			url : '../NarucivanjeHrane/api/logout',
			type : 'GET',
			success : function(){
				alert('Uspijesno izlogovani!');
				window.location = './index.html';
			}
		});
	}
}

function prikaziPodatke(){
	
	return function(event){
		event.preventDefault();
				
		$.ajax({
			url: 'api/user',
			type: 'GET',
			contentType : 'application/json',
			success : function(user){
					console.log(user);
					ispisiPodatke(user);
				
			}
		});
	}
	
}


function ispisiPodatke(user){	
	let div = $('#podaciDiv');

	let div2 = $('<div height:2000px; margin-top: 300px;"></div>');
	let tabela = $('<p style="color:#FEE12B;"><b>Korisnicko ime:</b></p>' + user.username + '<br><br>');
	let tabela2 = $('<p style="color:#FEE12B;"><b>Ime:</b></p><input type="text" id="imeEdit" value="' + user.firstName + '" /><br><br>');
	let tabela3 = $('<p  style="color:#FEE12B;"><b>Prezime:</b></p><input type="text" id="lastEdit" value="' + user.lastName + '" /><br><br>');
	let tabela4 = $('<p  style="color:#FEE12B;"><b>Lozinka:</b></p><input type="text" id="lozinkaEdit" value="' + user.password + '" /><br><br>');
	let tabela5 = $('<p  style="color:#FEE12B;"><b>Pol:</b></p><input type="text" id="polEdit" value="' + user.gender + '" /><br><br>');
	
	let button = $('<button id="izmeniButton" style="background-color: #FEE12B;">Izmeni podatke</button>');
	
	button.on('click', function(event){
	
		let newIme = document.getElementById('imeEdit').value;
		let newPrezime = document.getElementById('lastEdit').value;
		let newLozinka = document.getElementById('lozinkaEdit').value;
		let newPol = document.getElementById('polEdit').value;	
		
		if(newIme == null || newIme == undefined || newIme == ""){
			alert('Ime ne smije biti prazno!');
		}else if(newPrezime == null || newPrezime == undefined || newPrezime == ""){
			alert('Prezime ne smije biti prazno!');
		}else if(newLozinka == null || newLozinka == undefined || newLozinka == ""){
			alert('Lozinka ne smije biti prazna!');
		}else if(newPol == null || newPol == undefined || newPol == ""){
			alert('Pol ne smije biti prazan!');
		}else{
			izmena(newIme, newPrezime, newLozinka, newPol);
		}
	});
	
	
	div2.append(tabela).append(tabela2).append(tabela3).append(tabela4).append(tabela5).append(button);
	div.append(div2);
	
}

function izmena(ime, prezime, lozinka, pol){
	var obj = {"firstName": ime, "lastName": prezime, "password": lozinka, "gender": pol};
	$('#podaciDiv').html('');
	$.ajax({
		url: 'api/user/edit',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(result){
			ispisiPodatke(result);
			alert('Uspesno ste izmenili podatke')
		},
		error : function(){
			alert('Došlo je do greške prilikom izmene podataka!');
		}
	});
}

function prikaziRestorane(){
	return function(event){
		event.preventDefault();

		$('#restoraniDIV').html('');		
		$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/restaurant/allRestaurantsManager',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiRestorane(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza restorana!');
			}
		});
	}
}

function ispisiRestorane(restoran){
	let divR = $('#restoraniDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiPodatkeRestoranaDIV(restoran);
	let slika = ispisiSlikuDIV(restoran);
	let lokacija = ispisiLokaciju(restoran);
	
	div.append(podaci).append(slika).append(lokacija);
	divR.append(div);
}

function ispisiPodatkeRestoranaDIV(restoran){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let ime = $('<h2> <i> Restoran: </i>' + restoran.name + '</h2>');

	podaci.append(ime);
	return podaci;
}


function ispisiSlikuDIV(restoran){
	let slika = $('<div></div>');
	
	let img = $('<img src="' + restoran.logoPath + '" alt="" style="max-width: 100%; max-height: 100%;  display: block;margin-left: auto; margin-right: auto; width: 50%;"/>');
	slika.append(img);
	
	return slika;
	
}


function ispisiLokaciju(restoran){
	let lokacija = $('<div style="margin-left: 20px; margin-right: 20px; border-top: solid;background-color: #FEE12B; padding-left: 20px"></div>');
	let lokac = $('<h6><i><i> ' + restoran.location.longitude + ' LAT; ' + restoran.location.latitude + ' LONG' +  ' </i></h6>');
	
	let adresa = $('<h4><i> ' + restoran.location.address.street + ' ' + restoran.location.address.number +' '+ restoran.location.address.populatedPlace +' </i></h4>');
		
	lokacija.append(adresa).append(lokac);
	
	return lokacija;
}

function prikaziPorudzbine(){
	return function(event){
		event.preventDefault();

		$('#porudzbineDIV').html('');		
		$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/order/allOrdersManager',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiPorudzbine(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza porudzbina!');
			}
		});
	}
}

function ispisiPorudzbine(porudzbina){
	let divR = $('#porudzbineDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiPodatkePorudzbineDIV(porudzbina);

	
	div.append(podaci);
	divR.append(div);
}

function ispisiPodatkePorudzbineDIV(porudzbina){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let id = $('<h2> <i> Porudzbina : </i>' + porudzbina.id + '</h2>');
	let kupac = $('<h2> <i> Kupac: </i>' + porudzbina.buyer.firstName + ' '+porudzbina.buyer.lastName +'</h2>');
	let artikli = $('<h2> <i> Poručeni artikl: </i>' + porudzbina.articles.name + '</h2>');
	let kolicina =  $('<h2> <i> Količina: </i>' + porudzbina.articles.amount + '</h2>');
	let cena =  $('<h2> <i> Cena po komadu: </i>' + porudzbina.articles.price + '</h2>');
	
	
	podaci.append(id).append(kupac).append(artikli).append(kolicina).append(cena);
	return podaci;
}

function prikaziPoruceneArtikle(){
	return function(event){
		event.preventDefault();

		$('#artikliDIV').html('');		
		$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/article/allArticlesForOrder',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiArtikle(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza artikla!');
			}
		});
	}
}

function ispisiArtikle(artikl){
	let divR = $('#artikliDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiPodatkeArtiklaDIV(artikl);
	
	
	div.append(podaci);
	divR.append(div);
}

function ispisiPodatkeArtiklaDIV(artikl){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let name = $('<h2> <i> Poruceni artikl: </i>' + artikl.name + '</h2>');
	let price = $('<h2> <i> Cena artikla: </i>' + artikl.price  +'</h2>');
	
	podaci.append(name).append(price);
	return podaci;
}


function kreirajArtikl(){

	return function(event){
		event.preventDefault();
		
		$.ajax({
			type : 'GET',
			success : function(){
				window.location = './addArticles.html';
			}
		});
	}
}

function prikaziArtikl(){
	
	return function(event){
		event.preventDefault();
		$('#sviartikliDIV').html('');		
		$('#podaciDiv').html('');
				
		$.ajax({
			url: 'api/article/allArticlesForManager',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiSveArtikle(res);
				}
					

				
			}
		});
	}
	
}


function ispisiSveArtikle(artikl){
	let divR = $('#sviartikliDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiSveArtikleDIV(artikl);
	let slika = ispisiSlikuArtikla(artikl);
	
	div.append(slika).append(podaci);
	divR.append(div);
}

function ispisiSlikuArtikla(artikl){
	let slika = $('<div></div>');
	
	let img = $('<img src="' + artikl.imagePath + '" alt="" style="max-width: 100%; max-height: 100%;  display: block;margin-left: auto; margin-right: auto; width: 50%;"/>');
	slika.append(img);
	
	return slika;
	
}


function ispisiSveArtikleDIV(artikl){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let name = $('<h2> <i> Artikl: </i>' + artikl.name + '</h2>');
	let price = $('<h2> <i> Cena : </i>' + artikl.price  +'</h2>');
	let type = $('<h2> <i> Tip : </i>' + artikl.type  +'</h2>');
	
	
	podaci.append(name).append(price).append(type);
	return podaci;
}




function izmeniSveArtikle(){

	
	return function(event){
		event.preventDefault();
		$('#izmeniDIV').html('');		
		$('#podaciDiv').html('');
				
		$.ajax({
			url: 'api/article/allArticlesForManager',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
			for(let artikl of result){
					console.log(artikl);
					ispisiIzmenaArtikle(artikl);
			}	
			}
		});
	}
	
}



function ispisiIzmenaArtikle(artikl){	
	let div = $('#izmeniDIV');

	let div2 = $('<div height:2000px; margin-top: 300px;"></div>');
	let tabela = $('<p style="color:#FEE12B;"><b>Ime:</b></p><input type="text" id="imeEdit" value="' + artikl.name + '" /><br><br>');
	let tabela2 = $('<p style="color:#FEE12B;"><b>Cena:</b></p><input type="text" id="cenaEdit" value="' + artikl.price + '" /><br><br>');
	let tabela3 = $('<p  style="color:#FEE12B;"><b>Tip(DRINK/MEAL):</b></p><input type="text" id="tipEdit" value="' + artikl.type + '" /><br><br>');
	let tabela4 = $('<p  style="color:#FEE12B;"><b>Kolicina:</b></p><input type="text" id="kolicinaEdit" value="' + artikl.amount + '" /><br><br>');
	let tabela5 = $('<p  style="color:#FEE12B;"><b>Opis:</b></p><input type="text" id="opisEdit" value="' + artikl.description + '" /><br><br>');
	
	let button = $('<button id="izmeniButton" style="background-color: #FEE12B;">Izmeni podatke</button>');
	
	button.on('click', function(event){
	
		let newIme = document.getElementById('imeEdit').value;
		let newCena = document.getElementById('cenaEdit').value;
		let newTip = document.getElementById('tipEdit').value;
		//let newKolicina = document.getElementById('kolicinaEdit').value;	
		//let newOpis = document.getElementById('opisEdit').value;	
		
		
		if(newIme == null || newIme == undefined || newIme == ""){
			alert('Ime ne smije biti prazno!');
		}else if(newCena == null || newCena == undefined || newCena == ""){
			alert('Polje cena ne smije biti prazno!');
		}else if(newTip == null || newTip == undefined || newTip == ""){
			alert('Polje tip ne smije biti prazno!');
		}else{
			izmenaArtikala(newIme, newCena, newTip);
		}
	});
	
	
	div2.append(tabela).append(tabela2).append(tabela3).append(button);
	div.append(div2);
	
}

function izmenaArtikala(ime, cena, tip){
	var obj = {"name": ime, "price": cena, "type": tip};
	$('#izmeniDIV').html('');
	$.ajax({
		url: 'api/article/edit',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(result){
			ispisiSveArtikle(result);
			alert('Uspesno ste izmenili artikl')
		},
		error : function(){
			alert('Došlo je do greške prilikom izmene artikla!');
		}
	});
}


function prikazKomentara(){
	return function(event){
		event.preventDefault();

		$('#komentariDIV').html('');		
		//$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/comment/allAcceptM',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiKomentareAccept(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza komentara!');
			}
		});
	}
}

function ispisiKomentareAccept(komentar){
	let divR = $('#komentariDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	
	let podaci = ispisiAcceptKomentareDIV(komentar);
	let odobrava = ispisiOdobrava(komentar);
	let odbacuje = ispisiOdbacuje(komentar);
	
	div.append(podaci).append(odobrava).append(odbacuje);
	divR.append(div);
}

function ispisiOdbacuje(komentar){
	let button;
	if(komentar.status == "IN_PROCESS"){
		button = $('<button id="obradaKomentara' + komentar.id + '" style="margin-left: 20px; width: 90%;"> Odbij </button>');
		
		button.on('click', function(event){
			odbacujeKomentar(komentar);
		});
	}
	return button;
}

function odbacujeKomentar(komentar){
	var obj = {"id": komentar.id};
	
	$.ajax({
		url: 'api/comment/odbijen',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(response){
			alert('Komentar je odobren!');
		},
		error : function(response){
			alert('Ne mozete odobriti komentar!');
		}
	});
}

function ispisiOdobrava(komentar){
	let button;
	if(komentar.status == "IN_PROCESS"){
		button = $('<button id="obradaKomentara' + komentar.id + '" style="margin-left: 20px; width: 90%;"> Odobri </button>');
		
		button.on('click', function(event){
			odobriKomentar(komentar);
		});
	}
	return button;
}

function odobriKomentar(komentar){
	var obj = {"id": komentar.id};
	
	$.ajax({
		url: 'api/comment/odobren',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(response){
			alert('Komentar je odobren!');
		},
		error : function(response){
			alert('Ne mozete odobriti komentar!');
		}
	});
}

function ispisiAcceptKomentareDIV(komentar){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let name = $('<h3> <i> Restoran : </i>' + komentar.restaurant.name + '</h3>');
	let text = $('<h3> <i> Komentar: </i>' + komentar.text +'</h3>');
	let grade = $('<h3> <i> Ocena: </i>' + komentar.grade + '</h3>');
	let status = $('<h3> <i> Status: </i>' + komentar.status + '</h3>');
	
	podaci.append(name).append(text).append(grade).append(status);
	return podaci;
}
