$(document).ready(function(){
	

	$('#articlesForm').submit( (event)=>{
		event.preventDefault();
		
		var name = $('#name').val();
		var price = $('#price').val();
		var type = $('#type').val();
		var description = $('#description').val();
		var amount = $('#amount').val();
		var imagePath = $('#imagePath').val();

		if(name == "" || name == undefined || name == null){
			document.getElementById("alertName").innerHTML = "Niste uneli ime";
		}else if(price == "" || price == undefined || price == null){
			document.getElementById("alertName").innerHTML = "";
			document.getElementById("alertPrice").innerHTML = "Niste uneli cenu";
		}else if(type == "" || type == undefined || type == null){
			document.getElementById("alertName").innerHTML = "";
			document.getElementById("alertPrice").innerHTML = "";
			document.getElementById("alertTip").innerHTML = "Niste uneli tip";
		}else if(amount == "" || amount == undefined || amount == null){
			document.getElementById("alertName").innerHTML = "";
			document.getElementById("alertPrice").innerHTML = "";
			document.getElementById("alertTip").innerHTML = "";
		}else {
			var obj = { "name":$('#name').val(), "price" : $('#price').val(),
					"type" : $('#type').val(), "description" : $('#description').val(),
					"amount" : $('#amount').val(), "imagePath" : $('#imagePath').val()};
			   
			console.log(JSON.stringify(obj));
			$.ajax({
		    	contentType: 'application/json',
		        url: 'api/article/kreiraj',
		        type : 'POST',
		        data: JSON.stringify(obj),
		        success: function(response){
		        	alert('Artikl je uspesno kreniran.');
		        	console.log(response);
		        	window.location = './manager.html';	
		        }
		    });
		}
	});
})