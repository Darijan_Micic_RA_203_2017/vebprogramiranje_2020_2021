$(document).ready(function(){		
		
	document.getElementById('searchDiv').visibility = "hidden";
	
	
	$('#podaciDiv').html('');
		
	$('#pregledKorisnika').click(prikaziKorisnike());
	$('#pregledRestorana').click(prikaziRestorane());
	
	$('#kreirajRestoran').click(kreirajRestorane());
	$('#podaci').click(prikaziPodatke());
	$('#komentari').click(prikazKomentara());
	
	$('#logout').click(logout());
	
	$('#pretrazi').click(provjeriPretragu());
	
})

function logout(){
	return function(event){
		event.preventDefault();
		
		$.ajax({
			url : '../NarucivanjeHrane/api/logout',
			type : 'GET',
			success : function(){
				alert('Uspijesno izlogovani!');
				window.location = './index.html';
			}
		});
	}
}

function prikaziPodatke(){
	
	return function(event){
		event.preventDefault();
				
		$.ajax({
			url: 'api/user',
			type: 'GET',
			contentType : 'application/json',
			success : function(user){
					console.log(user);
					ispisiPodatke(user);
				
			}
		});
	}
	
}


function ispisiPodatke(user){	
	let div = $('#podaciDiv');

	let div2 = $('<div height:2000px; margin-top: 300px;"></div>');
	let naziv = $('<h3 style="color:#FEE12B;">Moji podaci:</h3>');
	let tabela = $('<p style="color:#FEE12B;"><b>Korisnicko ime:</b></p>' + user.username + '<br><br>');
	let tabela2 = $('<p style="color:#FEE12B;"><b>Ime:</b></p><input type="text" id="imeEdit" value="' + user.firstName + '" /><br><br>');
	let tabela3 = $('<p  style="color:#FEE12B;"><b>Prezime:</b></p><input type="text" id="lastEdit" value="' + user.lastName + '" /><br><br>');
	let tabela4 = $('<p  style="color:#FEE12B;"><b>Lozinka:</b></p><input type="text" id="lozinkaEdit" value="' + user.password + '" /><br><br>');
	let tabela5 = $('<p  style="color:#FEE12B;"><b>Pol:</b></p><input type="text" id="polEdit" value="' + user.gender + '" /><br><br>');
	
	let button = $('<button id="izmeniButton" style="background-color: #FEE12B;">Izmeni podatke</button>');
	
	button.on('click', function(event){
	
		let newIme = document.getElementById('imeEdit').value;
		let newPrezime = document.getElementById('lastEdit').value;
		let newLozinka = document.getElementById('lozinkaEdit').value;
		let newPol = document.getElementById('polEdit').value;	
		
		if(newIme == null || newIme == undefined || newIme == ""){
			alert('Ime ne smije biti prazno!');
		}else if(newPrezime == null || newPrezime == undefined || newPrezime == ""){
			alert('Prezime ne smije biti prazno!');
		}else if(newLozinka == null || newLozinka == undefined || newLozinka == ""){
			alert('Lozinka ne smije biti prazna!');
		}else if(newPol == null || newPol == undefined || newPol == ""){
			alert('Pol ne smije biti prazan!');
		}else{
			izmena(newIme, newPrezime, newLozinka, newPol);
		}
	});
	
	
	div2.append(naziv).append(tabela).append(tabela2).append(tabela3).append(tabela4).append(tabela5).append(button);
	div.append(div2);
	
}

function izmena(ime, prezime, lozinka, pol){
	var obj = {"firstName": ime, "lastName": prezime, "password": lozinka, "gender": pol};
	$('#podaciDiv').html('');
	$.ajax({
		url: 'api/user/edit',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(result){
			ispisiPodatke(result);
			alert('Uspesno ste izmenili podatke')
		},
		error : function(){
			alert('Došlo je do greške prilikom izmene podataka!');
		}
	});
}

function prikaziKorisnike(){
	
	return function(event){
		event.preventDefault();

		$('#korisniciDIV').html('');
		//$('#podaciDiv').html('');
		$.ajax({
			url: 'api/user/allUsers',
			type: 'GET',
			contentType : 'application/json',
			success : function(users){			
				ispisiKorisnike(users);
				
			}
		});
	}
}

function ispisiKorisnike(users){
	
	let div = $('#korisniciDIV');
	let div2 = $('<div height:2000px; margin-top: 300px;"></div>');
	let pretraga = ispisiPretraguKorisnika();
	let naziv = $('<h3>Pregled svih korisnika:</h3>')
	let tabela = $('<table border="1"><th>Korisnicko Ime</th><th>Ime</th><th>Prezime</th></table>');
	for(let user of users){
		let tr = $('<tr></tr>');
		let username = $('<td>' + user.username + '</td>');
		let ime = $('<td>' + user.firstName + '</td>');
		let prezime = $('<td>' + user.lastName + '</td>');
		tr.append(username).append(ime).append(prezime);
		tabela.append(tr);
	}
	
	div2.append(naziv).append(tabela).append(pretraga);
	div.append(div2);
}

function ispisiPretraguKorisnika(){
	var div = $('<div><div>');
	let pUsername = $('<p>Unesite korisnicko ime:</p>');
	let inputUsername = $('<input id="korisnickoIme" type="text" placeholder="korisnicko ime"/>');
	
	let pImee = $('<p>Unesite ime:</p>');
	let inputIme = $('<input id="ime" type="text" placeholder="ime"/>');
	let pPrezimee = $('<p>Unesite prezime:</p>');
	let inputPrezime = $('<input id="prezime" type="text" placeholder="prezime"/>');
	
	let button = $('<br><button>Pretrazi</button>');
	
	button.on('click', function(event){
		var usernameText = document.getElementById('korisnickoIme').value;
		var imeText = document.getElementById('ime').value;
		var prezimeText = document.getElementById('prezime').value;
		
		pretraziKorisnike(usernameText, imeText, prezimeText);
	});
	
	div.append(pUsername).append(inputUsername).append(pImee).append(inputIme).append(pPrezimee).append(inputPrezime).append(button);
	return div;
}

function pretraziKorisnike(usernameText, imeText, prezimeText){
	$('#korisniciDIV').html('');
	
	var obj = {"username": usernameText, "firstName": imeText, "lastName": prezimeText};
	
	$.ajax({
		url: 'api/user/admin/users/search',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(response){
			ispisiKorisnike(response);
		},
		error : function(response){
			console.log('Greska prilikom pretrage korisnika!');
		}
	});
}


function kreirajRestorane(){

	return function(event){
		event.preventDefault();
		
		$.ajax({
			type : 'GET',
			success : function(){
				window.location = './restaurant.html';
			}
		});
	}
}


function prikaziRestorane(){
	
	return function(event){
		event.preventDefault();

		$('#restoraniDIV').html('');
		$('#podaciDiv').html('');
		//document.getElementById('searchDiv').style.visibility = "visible";
		
		$.ajax({
			url: 'api/restaurant/allRestaurants',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){			
				for(let restaurant of result){
					console.log(restaurant);
					ispisiRestorane(restaurant);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza restorana!');
			}
		});
	}
}

function ispisiRestorane(restoran){
	let divR = $('#restoraniDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiPodatkeRestoranaDIV(restoran);
	let slika = ispisiSlikuDIV(restoran);
	let lokacija = ispisiLokaciju(restoran);
	
	div.append(podaci).append(slika).append(lokacija);
	divR.append(div);
}

function ispisiPodatkeRestoranaDIV(restoran){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let ime = $('<h2> <i> Restoran: </i>' + restoran.name + '</h2>');

	podaci.append(ime);
	return podaci;
}


function ispisiSlikuDIV(restoran){
	let slika = $('<div></div>');
	
	let img = $('<img src="' + restoran.logoPath + '" alt="" style="max-width: 100%; max-height: 100%;  display: block;margin-left: auto; margin-right: auto; width: 50%;"/>');
	slika.append(img);
	
	return slika;
	
}


function ispisiLokaciju(restoran){
	let lokacija = $('<div style="margin-left: 20px; margin-right: 20px; border-top: solid;background-color: #FEE12B; padding-left: 20px"></div>');
	let lokac = $('<h6><i><i> ' + restoran.location.longitude + ' LAT; ' + restoran.location.latitude + ' LONG' +  ' </i></h6>');
	
	let adresa = $('<h4><i> ' + restoran.location.address.street + ' ' + restoran.location.address.number +' '+ restoran.location.address.populatedPlace +' </i></h4>');
		
	lokacija.append(adresa).append(lokac);
	
	return lokacija;
}


function provjeriPretragu(){
	return function(event){
		event.preventDefault();
		
		$('#podaciDiv').html('');
		
		var naziv;
		var cena;
		
		var nazivElem = document.getElementById('inputNaziv').value;
		if(nazivElem == undefined || nazivElem == null){
			naziv = "";
		}else{
			naziv = nazivElem;
		}
		var tip;
		var tipElem = document.getElementById('inputTip').value;
		if(tipElem == undefined || tipElem == null){
			tip = "";
		}else{
			tip = tipElem; 
		}
		
		
		var mesto;
		var mestoElem = document.getElementById('inputMesto').value;
		if(mestoElem == undefined || mestoElem == null){
			mesto = "";
		}else{
			mesto = mestoElem; 
		}
		
		var ulica;
		var ulicaElem = document.getElementById('inputUlica').value;
		if(ulicaElem == undefined || ulicaElem == null){
			ulica= "";
		}else{
			ulica = ulicaElem; 
		}
		
		
		
		var aktivanElem = document.getElementById('aktivan').checked;
		var neaktivanElem = document.getElementById('neaktivan').checked;
		var sortirajStatus;
		if(aktivanElem == true){
			sortirajStatus = "0";
		}else if(neaktivanElem){
			sortirajStatus = "1";
		}else{
			sortirajStatus = "";
		}
		
		pretraziRestorane(naziv, tip, mesto, ulica, sortirajStatus);
		
		
	}
}

function pretraziRestorane(naziv, tip, mesto, ulica, sortirajStatus){
	
	var obj = {"name": naziv, "type":{ "name" : tip}, "location":{ "address": {"populatedPlace": mesto, "street": ulica}}, "sortirajStatus":sortirajStatus};
	
	
	$.ajax({
		url: 'api/restaurant/search',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(response){
			for(let res of response){
				console.log(res);
				ispisiRestorane(res);
			}
		},
		error : function(response){
			console.log('Greska prilikom pretrage!');
		}
	});
}


function prikazKomentara(){
	return function(event){
		event.preventDefault();

		$('#komentariDIV').html('');		
		//$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/comment/allComments',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiKomentareAccept(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza komentara!');
			}
		});
	}
}

function ispisiKomentareAccept(komentar){
	let divR = $('#komentariDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	
	let podaci = ispisiAcceptKomentareDIV(komentar);
	let obrisi = ispisiButtonObrisi(komentar);
	
	div.append(podaci).append(obrisi);
	divR.append(div);
}

function ispisiButtonObrisi(komentar){

	let button;
	if(!komentar.obrisan){
		button = $('<button id="obrisiKomentar' + komentar.id + '" style="margin-left: 20px; width: 90%;"> Obrisi </button>');
		
		button.on('click', function(event){
			obrisi(komentar.id);
		});
	}
	
	return button;
}
function obrisi(id){
	var obj = {"id": id};
	
	
	$.ajax({
		url: 'api/comment',
		type: 'DELETE',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(response){
			alert('Uspesno ste obrisali komentar');
		},
		error : function(response){
			alert('Ne mozete trenutno obrisati komentar.');
		}
	});
	
}

function ispisiAcceptKomentareDIV(komentar){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let name = $('<h3> <i> Restoran : </i>' + komentar.restaurant.name + '</h3>');
	let text = $('<h3> <i> Komentar: </i>' + komentar.text +'</h3>');
	let grade = $('<h3> <i> Ocena: </i>' + komentar.grade + '</h3>');
	let status = $('<h3> <i> Status: </i>' + komentar.status + '</h3>');
	
	podaci.append(name).append(text).append(grade).append(status);
	return podaci;
}
