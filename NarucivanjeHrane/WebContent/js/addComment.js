$(document).ready(function(){
	
	$('#commentForm').submit( (event)=>{
		event.preventDefault();

		var restName = $('#restName').val();		
		var text = $('#text').val();
		var type = $('#type').val();

		if(restName == "" || restName == undefined || restName == null){
			document.getElementById("alertRest").innerHTML = "Niste uneli restoran";
		}else if(text == "" || text == undefined || text == null){
			document.getElementById("alertRest").innerHTML = "";
			document.getElementById("alertCom").innerHTML = "Niste uneli komentar";
		}else if(type == "" || type == undefined || type == null){
			document.getElementById("alertRest").innerHTML = "";
			document.getElementById("alertCom").innerHTML = "";
			document.getElementById("alertTip").innerHTML = "Niste uneli ocenu";
		}else {
			var obj = { "restaurant" : {"name": $('#restName').val()},
					"text":$('#text').val(), 
					"grade" : $('#type').val()};
			   
			console.log(JSON.stringify(obj));
			$.ajax({
		    	contentType: 'application/json',
		        url: 'api/comment/dodaj',
		        type : 'POST',
		        data: JSON.stringify(obj),
		        success: function(response){
		        	alert('Komentar je uspesno kreniran.');
		        	console.log(response);
		        	window.location = './buyer.html';	
		        }
		    });
		}
	});
})