$(document).ready(function(){

	$('#podaciDiv').html('');
	$('#zaduzene').click(prikazPorudzbina());
	$('#nedostavljene').click(prikazNedostavljenih());
	$('#ceka').click(prikazCeka());
	$('#podaci').click(prikaziPodatke());
	$('#logout').click(logout());
})

function logout(){
	return function(event){
		event.preventDefault();
		
		$.ajax({
			url : '../NarucivanjeHrane/api/logout',
			type : 'GET',
			success : function(){
				alert('Uspijesno izlogovani!');
				window.location = './index.html';
			}
		});
	}
}

function prikaziPodatke(){
	
	return function(event){
		event.preventDefault();
				
		$.ajax({
			url: 'api/user',
			type: 'GET',
			contentType : 'application/json',
			success : function(user){
					console.log(user);
					ispisiPodatke(user);
			}
		});
	}
	
}


function ispisiPodatke(user){	
	let div = $('#podaciDiv');

	let div2 = $('<div height:2000px; margin-top: 300px;"></div>');
	let tabela = $('<p style="color:#FEE12B;"><b>Korisnicko ime:</b></p>' + user.username + '<br><br>');
	let tabela2 = $('<p style="color:#FEE12B;"><b>Ime:</b></p><input type="text" id="imeEdit" value="' + user.firstName + '" /><br><br>');
	let tabela3 = $('<p  style="color:#FEE12B;"><b>Prezime:</b></p><input type="text" id="lastEdit" value="' + user.lastName + '" /><br><br>');
	let tabela4 = $('<p  style="color:#FEE12B;"><b>Lozinka:</b></p><input type="text" id="lozinkaEdit" value="' + user.password + '" /><br><br>');
	let tabela5 = $('<p  style="color:#FEE12B;"><b>Pol:</b></p><input type="text" id="polEdit" value="' + user.gender + '" /><br><br>');
	
	let button = $('<button id="izmeniButton" style="background-color: #FEE12B;">Izmeni podatke</button>');
	
	button.on('click', function(event){
	
		let newIme = document.getElementById('imeEdit').value;
		let newPrezime = document.getElementById('lastEdit').value;
		let newLozinka = document.getElementById('lozinkaEdit').value;
		let newPol = document.getElementById('polEdit').value;	
		
		if(newIme == null || newIme == undefined || newIme == ""){
			alert('Ime ne smije biti prazno!');
		}else if(newPrezime == null || newPrezime == undefined || newPrezime == ""){
			alert('Prezime ne smije biti prazno!');
		}else if(newLozinka == null || newLozinka == undefined || newLozinka == ""){
			alert('Lozinka ne smije biti prazna!');
		}else if(newPol == null || newPol == undefined || newPol == ""){
			alert('Pol ne smije biti prazan!');
		}else{
			izmena(newIme, newPrezime, newLozinka, newPol);
		}
	});
	
	
	div2.append(tabela).append(tabela2).append(tabela3).append(tabela4).append(tabela5).append(button);
	div.append(div2);
	
}

function izmena(ime, prezime, lozinka, pol){
	var obj = {"firstName": ime, "lastName": prezime, "password": lozinka, "gender": pol};
	$('#podaciDiv').html('');
	$.ajax({
		url: 'api/user/edit',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(result){
			ispisiPodatke(result);
			alert('Uspesno ste izmenili podatke')
		},
		error : function(){
			alert('Došlo je do greške prilikom izmene podataka!');
		}
	});
}

function prikazPorudzbina(){
	return function(event){
		event.preventDefault();

		$('#zaduzeneDIV').html('');		
		$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/order/allOrdersD',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiPorudzbine(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza porudzbina!');
			}
		});
	}
}

function ispisiPorudzbine(porudzbina){
	let divR = $('#zaduzeneDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiPodatkePorudzbineDIV(porudzbina);

	
	div.append(podaci);
	divR.append(div);
}

function ispisiPodatkePorudzbineDIV(porudzbina){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let id = $('<h3> <i> Porudzbina : </i>' + porudzbina.id + '</h3>');
	let k = $('<h3> <i> Status porudzbine: </i>' + porudzbina.status  +'</h3>');
	let artikli = $('<h3> <i> Poručeni artikl: </i>' + porudzbina.articles.name + '</h3>');
	let kolicina =  $('<h3> <i> Količina: </i>' + porudzbina.articles.amount + '</h3>');
	let cena =  $('<h3> <i> Cena po komadu: </i>' + porudzbina.articles.price + '</h3>');
	
	
	podaci.append(id).append(k).append(artikli).append(kolicina).append(cena);
	return podaci;
}

function prikazNedostavljenih(){
	return function(event){
		event.preventDefault();

		$('#nedostavljeneDIV').html('');		
		$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/order/allOrdersDNedostavljene',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiNPorudzbine(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza porudzbina!');
			}
		});
	}
}

function ispisiNPorudzbine(porudzbina){
	let divR = $('#nedostavljeneDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiNPodatkePorudzbineDIV(porudzbina);

	
	div.append(podaci);
	divR.append(div);
}

function ispisiNPodatkePorudzbineDIV(porudzbina){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let id = $('<h3> <i> Porudzbina : </i>' + porudzbina.id + '</h3>');
	let k = $('<h3> <i> Status porudzbine: </i>' + porudzbina.status +'</h3>');
	let artikli = $('<h3> <i> Poručeni artikl: </i>' + porudzbina.articles.name + '</h3>');
	let kolicina =  $('<h3> <i> Količina: </i>' + porudzbina.articles.amount + '</h3>');
	let cena =  $('<h3> <i> Cena po komadu: </i>' + porudzbina.articles.price + '</h3>');
	
	
	podaci.append(id).append(k).append(artikli).append(kolicina).append(cena);
	return podaci;
}

function prikazCeka(){
	return function(event){
		event.preventDefault();

		$('#cekaDIV').html('');		
		$('#podaciDiv').html('');
	
		$.ajax({
			url: 'api/order/allOrdersDCeka',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){
				for(let res of result){
					console.log(res);
					ispisiCekaPorudzbine(res);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza porudzbina!');
			}
		});
	}
}

function ispisiCekaPorudzbine(porudzbina){
	let divR = $('#cekaDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiCekaPodatkePorudzbineDIV(porudzbina);
	let zatrazi = ispisiDugmeZatrazi(porudzbina);

	
	div.append(podaci).append(zatrazi);
	divR.append(div);
}

function ispisiDugmeZatrazi(porudzbina){
	let button;
	if(porudzbina.status == "WAITING_FOR_DELIVERER"){
		button = $('<button id="obradaPorudzbine' + porudzbina.id + '" style="margin-left: 20px; width: 90%;"> Zatrazi dostavu </button>');
		
		button.on('click', function(event){
			zatrazi(porudzbina);
		});
	}
	return button;
}

function zatrazi(porudzbina){
	var obj = {"id": porudzbina.id};
	
	$.ajax({
		url: 'api/order/status/zatrazi',
		type: 'POST',
		contentType : 'application/json',
		data: JSON.stringify(obj),
		success : function(response){
			alert('Zatrazili ste dostavu porudzbine');
		},
		error : function(response){
			alert('Ne mozete zatraziti dostavu porudzbine!');
		}
	});
}

function ispisiCekaPodatkePorudzbineDIV(porudzbina){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let id = $('<h3> <i> Porudzbina : </i>' + porudzbina.id + '</h3>');
	let k = $('<h3> <i> Status porudzbine: </i>' + porudzbina.status +'</h3>');
	let artikli = $('<h3> <i> Poručeni artikl: </i>' + porudzbina.articles.name + '</h3>');
	let kolicina =  $('<h3> <i> Količina: </i>' + porudzbina.articles.amount + '</h3>');
	let cena =  $('<h3> <i> Cena po komadu: </i>' + porudzbina.articles.price + '</h3>');
	
	
	podaci.append(id).append(k).append(artikli).append(kolicina).append(cena);
	return podaci;
}