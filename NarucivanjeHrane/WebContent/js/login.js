$(document).ready(function(){
	
	$('#loginForm').submit( (event)=>{
		event.preventDefault();
		

		var username = $('#loginUsername').val();
		let password = $('#loginPassword').val();

		if(username == "" || username == undefined || username == null){
			document.getElementById("alertUsername").innerHTML = "Niste uneli korisnicko ime!";
			document.getElementById("alertPassword").innerHTML = "";
		}else if(password == "" || password == undefined || password == null){
			document.getElementById("alertUsername").innerHTML = "";
			document.getElementById("alertPassword").innerHTML = "Niste uneli lozinku!";
		}else{
			var obj = {"username":username, "password" : password};
			console.log(obj);
			
			$.ajax({
	        	contentType: 'application/json',
	            url: '../NarucivanjeHrane/api/login',
	            type : 'POST',
	            data: JSON.stringify(obj),
	            success: function(response){
	            	if(response==null){
	            		console.log('NULL');
	            		console.log(response);
	            		alert('Pogresno Korisnicko ime ili Lozinka.');
	            		//document.getElementById("messageLogin").className = '';
	            	}else{
	            		console.log('Success in login!');
	            		
	            		if(response.role == "BUYER"){
	            			window.location = './buyer.html';
	            		}else if(response.role == "ADMINISTRATOR"){
	            			window.location = './administrator.html';
	            		}else if(response.role == "MANAGER"){
	            			window.location = './manager.html';
	            		}else if (response.role == "DELIVERER"){
	            			window.location = './deliverer.html';
	            		}
	            		
	            	}

	              }
	          });
		}
		
		
		
	  });

})