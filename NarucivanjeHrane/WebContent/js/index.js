$(document).ready(function(){		
	
	$('#podaciDiv').html('');

	$('#pregledRestorana').click(prikaziRestorane());

})

function prikaziRestorane(){
	
	return function(event){
		event.preventDefault();

		$('#restoraniDIV').html('');
		$('#podaciDiv').html('');
		$.ajax({
			url: 'api/restaurant/allRestaurants',
			type: 'GET',
			contentType : 'application/json',
			success : function(result){			
				for(let restaurant of result){
					console.log(restaurant);
					ispisiRestorane(restaurant);
				}
			},
			error : function(){
				alert('Došlo je do greške prilikom prikaza restorana!');
			}
		});
	}
}

function ispisiRestorane(restoran){
	let divR = $('#restoraniDIV');
	let div = $('<div style="border-style: solid; border-width: medium; margin-top: 20px; background-color:black;"></div>');
	
	let podaci = ispisiPodatkeRestoranaDIV(restoran);
	let slika = ispisiSlikuDIV(restoran);
	let lokacija = ispisiLokaciju(restoran);
	
	div.append(podaci).append(slika).append(lokacija);
	divR.append(div);
}

function ispisiPodatkeRestoranaDIV(restoran){

	let podaci = $('<div style="margin-left: 20px; margin-right: 20px;background-color:#FEE12B;"></div>');
	
	let ime = $('<h2> <i> Restoran: </i>' + restoran.name + '</h2>');

	podaci.append(ime);
	return podaci;
}


function ispisiSlikuDIV(restoran){
	let slika = $('<div></div>');
	
	let img = $('<img src="' + restoran.logoPath + '" alt="" style="max-width: 100%; max-height: 100%;  display: block;margin-left: auto; margin-right: auto; width: 50%;"/>');
	slika.append(img);
	
	return slika;
	
}


function ispisiLokaciju(restoran){
	let lokacija = $('<div style="margin-left: 20px; margin-right: 20px; border-top: solid;background-color: #FEE12B; padding-left: 20px"></div>');
	let lokac = $('<h6><i><i> ' + restoran.location.longitude + ' LAT; ' + restoran.location.latitude + ' LONG' +  ' </i></h6>');
	
	let adresa = $('<h4><i> ' + restoran.location.address.street + ' ' + restoran.location.address.number +' '+ restoran.location.address.populatedPlace +' </i></h4>');
		
	lokacija.append(adresa).append(lokac);
	
	return lokacija;
}