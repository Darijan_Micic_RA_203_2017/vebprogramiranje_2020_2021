package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {

	private long id;

	private String username;
	private String password;
	private String firstName;
	private String lastName;
	//private int role = 0; //0-Kupac, 1-Admin, 2-Menadzer, 3-Dostavljac
	
	private Gender gender;
	private Date dateOfBirth;
	private UserRole role;
	private ArrayList<Order> boughtOrders;
	private ShoppingCart shoppingCart;
	private Restaurant restaurant;
	private ArrayList<Order> ordersInNeedOfDelivery;
	private double collectedPoints;
	private BuyerType buyerType;
	
	private List<Long> rest = new ArrayList<Long>();
	
	public List<Long> getRest() {
		return rest;
	}

	public void setRest(List<Long> rest) {
		this.rest = rest;
	}

	public User(Long id, String username, String password, String firstName, String lastName, Gender gender, UserRole role ) {
		super();
		this.id =  id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.role = role;
	}

	public User() {
		
	}
	
	public User(long id, String username, String password, String firstName, 
			String lastName, Gender gender, Date dateOfBirth, UserRole role, 
			ArrayList<Order> boughtOrders, ShoppingCart shoppingCart, 
			Restaurant restaurant, ArrayList<Order> ordersInNeedOfDelivery, 
			int collectedPoints, BuyerType buyerType) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.role = role;
		this.boughtOrders = boughtOrders;
		this.shoppingCart = shoppingCart;
		this.restaurant = restaurant;
		this.ordersInNeedOfDelivery = ordersInNeedOfDelivery;
		this.collectedPoints = collectedPoints;
		this.buyerType = buyerType;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public ArrayList<Order> getBoughtOrders() {
		return boughtOrders;
	}

	public void setBoughtOrders(ArrayList<Order> boughtOrders) {
		this.boughtOrders = boughtOrders;
	}

	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public ArrayList<Order> getOrdersInNeedOfDelivery() {
		return ordersInNeedOfDelivery;
	}

	public void setOrdersInNeedOfDelivery(ArrayList<Order> ordersInNeedOfDelivery) {
		this.ordersInNeedOfDelivery = ordersInNeedOfDelivery;
	}

	public double getCollectedPoints() {
		return collectedPoints;
	}

	public void setCollectedPoints(double collectedPoints) {
		this.collectedPoints = collectedPoints;
	}

	public BuyerType getBuyerType() {
		return buyerType;
	}

	public void setBuyerType(BuyerType buyerType) {
		this.buyerType = buyerType;
	}
}
