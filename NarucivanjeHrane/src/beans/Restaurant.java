package beans;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {
	private long id;
	private String name;
	private RestaurantType type;
	private ArrayList<Article> articles;
	private RestaurantStatus status;
	private Location location;
	private String logoPath;
		
	private User manager;
	private User admin;
	
	public User getMaganer() {
		return manager;
	}

	public void setMaganer(User manager) {
		this.manager = manager;
	}

	public Restaurant() {}
	
	public Restaurant(long id, String name, RestaurantType type, 
			ArrayList<Article> articles, RestaurantStatus status, Location location, 
			String logoPath) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.articles = articles;
		this.status = status;
		this.location = location;
		this.logoPath = logoPath;
	}
	
	public Restaurant(Restaurant r) {
		this.id = r.id;
		this.name = r.name;
		this.type = r.type;
		this.articles = r.articles;
		this.status = r.status;
		this.location = r.location;
		this.logoPath = r.logoPath;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RestaurantType getType() {
		return type;
	}

	public void setType(RestaurantType type) {
		this.type = type;
	}

	public ArrayList<Article> getArticles() {
		return articles;
	}

	public void setArticles(ArrayList<Article> articles) {
		this.articles = articles;
	}

	public RestaurantStatus getStatus() {
		return status;
	}

	public void setStatus(RestaurantStatus status) {
		this.status = status;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}
	
	public void setAdmin(User admin) {
		this.admin = admin;
	}
}
