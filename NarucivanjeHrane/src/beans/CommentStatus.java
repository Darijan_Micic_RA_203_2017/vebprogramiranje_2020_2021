package beans;

public enum CommentStatus {
	IN_PROCESS, ACCEPT, DECLINE;
	
	private static final String[] descriptions = {"Obrada", "Odobren", "Odbijen"};
	
	public String toString() {
		return descriptions[ordinal()];
	}

}
