package beans;

import java.util.ArrayList;
import java.util.Date;

public class Order {
	private Long id;
	//private ArrayList<Article> articles = new ArrayList<Article>();
	private Article articles;
	private Restaurant restaurant;
	private Date dateAndTime;
	private double totalPrice;
	private User buyer;
	private OrderStatus status;
	private User deliverer;
	
	private int req = 0;
	
	public Order() {}
	
	public Order(Long id, Article articles, Restaurant restaurant, 
			Date dateAndTime, double totalPrice, User buyer, User deliverer, OrderStatus status) {
		this.id = id;
		this.articles = articles;
		this.restaurant = restaurant;
		this.dateAndTime = dateAndTime;
		this.totalPrice = totalPrice;
		this.buyer = buyer;
		this.deliverer = deliverer;
		this.status = status;
	}
	
	
	
	public int getReq() {
		return req;
	}

	public void setReq(int req) {
		this.req = req;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Article getArticles() {
		return articles;
	}

	public void setArticles(Article articles) {
		this.articles = articles;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public Date getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public User getBuyer() {
		return buyer;
	}

	public void setBuyer(User buyer) {
		this.buyer = buyer;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public User getDeliverer() {
		return deliverer;
	}

	public void setDeliverer(User deliverer) {
		this.deliverer = deliverer;
	}
	
	
}
