package beans;

import java.util.ArrayList;

public class ShoppingCart {
	private long id;
	private ArrayList<Article> articles;
	private User buyer;
	private double totalPrice;
	
	public ShoppingCart() {}
	
	public ShoppingCart(long id, ArrayList<Article> articles, User buyer, 
			double totalPrice) {
		this.id = id;
		this.articles = articles;
		this.buyer = buyer;
		this.totalPrice = totalPrice;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public ArrayList<Article> getArticles() {
		return articles;
	}

	public void setArticles(ArrayList<Article> articles) {
		this.articles = articles;
	}

	public User getBuyer() {
		return buyer;
	}

	public void setBuyer(User buyer) {
		this.buyer = buyer;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
}
