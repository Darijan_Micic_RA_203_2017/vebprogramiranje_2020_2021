package beans;

public class Address {
	private long id;
	private String street;
	private String number;
	private String populatedPlace;
	private int postalCode;
	
	public Address() {}
	
	public Address(long id, String street, String number, String populatedPlace, 
			int postalCode) {
		this.id = id;
		this.street = street;
		this.number = number;
		this.populatedPlace = populatedPlace;
		this.postalCode = postalCode;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPopulatedPlace() {
		return populatedPlace;
	}

	public void setPopulatedPlace(String populatedPlace) {
		this.populatedPlace = populatedPlace;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}
}
