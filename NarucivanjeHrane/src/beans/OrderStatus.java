package beans;

public enum OrderStatus {
	IN_PROCESS, IN_PREPARATION, WAITING_FOR_DELIVERER, IN_TRANSPORT, DELIVERED, CANCELLED;
	
	private static final String[] descriptions = {"Obrada", "U pripremi", 
			"Čeka dostavljača", "U transportu", "Dostavljena", "Otkazana"};
	
	public String toString() {
		return descriptions[ordinal()];
	}
}
