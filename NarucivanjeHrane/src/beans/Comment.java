package beans;

import java.util.UUID;

public class Comment {
	private Long id;
	private User buyer;
	private Restaurant restaurant;
	private String text;
	private int grade;
	private CommentStatus status;
	private boolean obrisan = false;
	
	public Comment() {}
	
	public Comment(Long id, User buyer, Restaurant restaurant, String text, int grade, CommentStatus status ) {
		this.id = id;
		this.buyer = buyer;
		this.restaurant = restaurant;
		this.text = text;
		this.grade = grade;
		this.status = status;
	}
	
	public boolean isObrisan() {
		return obrisan;
	}

	public void setObrisan(boolean obrisan) {
		this.obrisan = obrisan;
	}
	
	public CommentStatus getStatus() {
		return status;
	}

	public void setStatus(CommentStatus status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public User getBuyer() {
		return buyer;
	}

	public void setBuyer(User buyer) {
		this.buyer = buyer;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}
}
