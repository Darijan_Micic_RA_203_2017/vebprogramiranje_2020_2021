package beans;

public enum RestaurantStatus {
	ACTIVE, NOT_ACTIVE;
	
	private static final String[] descriptions = {"Radi", "Ne radi"};
	
	public String toString() {
		return descriptions[ordinal()];
	}
}
