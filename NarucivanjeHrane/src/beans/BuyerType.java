package beans;

public class BuyerType {
	private long id;
	private String name;
	private double discountPercentage;
	private int neededNumberOfCollectedPoints;
	
	public BuyerType() {}
	
	public BuyerType(long id, String name, double discountPercentage, 
			int neededNumberOfCollectedPoints) {
		this.id = id;
		this.name = name;
		this.discountPercentage = discountPercentage;
		this.neededNumberOfCollectedPoints = neededNumberOfCollectedPoints;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public int getNeededNumberOfCollectedPoints() {
		return neededNumberOfCollectedPoints;
	}

	public void setNeededNumberOfCollectedPoints(int neededNumberOfCollectedPoints) {
		this.neededNumberOfCollectedPoints = neededNumberOfCollectedPoints;
	}
}
