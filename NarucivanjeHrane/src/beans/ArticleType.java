package beans;

public enum ArticleType {
	MEAL, DRINK;
	
	private static final String[] descriptions = {"jelo", "piće"};
	
	public String toString() {
		return descriptions[ordinal()];
	}
}
