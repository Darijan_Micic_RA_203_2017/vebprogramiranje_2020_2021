package beans;

public enum UserRole {
	ADMINISTRATOR, MANAGER, DELIVERER, BUYER;
	
	private static final String[] descriptions = {"Administrator", "Menadžer", 
			"Dostavljač", "Kupac"};
	
	public String toString() {
		return descriptions[ordinal()];
	}
}
