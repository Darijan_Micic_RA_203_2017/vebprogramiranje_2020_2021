package beans;

public enum Gender {
	MALE, FEMALE;
	
	private static final String[] descriptions = {"Muški", "Ženski"};
	
	public String toString() {
		return descriptions[ordinal()];
	}
}
