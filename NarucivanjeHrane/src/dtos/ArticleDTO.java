package dtos;

import beans.Article;
import beans.ArticleType;

public class ArticleDTO {
	private long id;
	private String name;
	private double price;
	private ArticleType type;
	private long restaurantId;
	private double amount;
	private String description;
	private String imagePath;
	
	public ArticleDTO() {}
	
	public ArticleDTO(long id, String name, double price, ArticleType type, 
			long restaurantId, double amount, String description, String imagePath) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.type = type;
		this.restaurantId = restaurantId;
		this.amount = amount;
		this.description = description;
		this.imagePath = imagePath;
	}
	
	public ArticleDTO(Article article) {
		this.id = article.getId();
		this.name = article.getName();
		this.price = article.getPrice();
		this.type = article.getType();
		this.restaurantId = article.getRestaurant().getId();
		this.amount = article.getAmount();
		this.description = article.getDescription();
		this.imagePath = article.getImagePath();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ArticleType getType() {
		return type;
	}

	public void setType(ArticleType type) {
		this.type = type;
	}

	public long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
}
