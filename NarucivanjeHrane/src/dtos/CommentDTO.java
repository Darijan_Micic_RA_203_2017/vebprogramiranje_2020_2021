package dtos;

import beans.Comment;

public class CommentDTO {
	private long id;
	private long buyerId;
	private long restaurantId;
	private String text;
	private int grade;
	
	public CommentDTO() {}
	
	public CommentDTO(long id, long buyerId, long restaurantId, String text, int grade) {
		this.id = id;
		this.buyerId = buyerId;
		this.restaurantId = restaurantId;
		this.text = text;
		this.grade = grade;
	}
	
	public CommentDTO(Comment comment) {
		//this.id = comment.getId();
		this.buyerId = comment.getBuyer().getId();
		this.restaurantId = comment.getRestaurant().getId();
		this.text = comment.getText();
		this.grade = comment.getGrade();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(long buyerId) {
		this.buyerId = buyerId;
	}

	public long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}
}
