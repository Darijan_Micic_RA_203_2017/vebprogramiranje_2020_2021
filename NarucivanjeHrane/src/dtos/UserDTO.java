package dtos;

import java.util.ArrayList;
import java.util.Date;

import beans.Gender;
import beans.Order;
import beans.User;
import beans.UserRole;

public class UserDTO {
	private long id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private Gender gender;
	private Date dateOfBirth;
	private UserRole role;
	private ArrayList<String> boughtOrdersIds;
	private long shoppingCartId;
	private long restaurantId;
	private ArrayList<String> ordersInNeedOfDeliveryIds;
	private double collectedPoints;
	private long buyerTypeId;
	
	public UserDTO() {}
	
	public UserDTO(long id, String username, String password, String firstName, 
			String lastName, Gender gender, Date dateOfBirth, UserRole role, 
			ArrayList<String> boughtOrdersIds, long shoppingCartId, long restaurantId, 
			ArrayList<String> ordersInNeedOfDeliveryIds, int collectedPoints, 
			long buyerTypeId) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.role = role;
		this.boughtOrdersIds = boughtOrdersIds;
		this.shoppingCartId = shoppingCartId;
		this.restaurantId = restaurantId;
		this.ordersInNeedOfDeliveryIds = ordersInNeedOfDeliveryIds;
		this.collectedPoints = collectedPoints;
		this.buyerTypeId = buyerTypeId;
	}
	
	public UserDTO(User user) {
		this.id = user.getId();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.gender = user.getGender();
		this.dateOfBirth = user.getDateOfBirth();
		this.role = user.getRole();
		this.boughtOrdersIds = new ArrayList<String>();
		/*for (Order o : user.getBoughtOrders()) {
			this.boughtOrdersIds.add(o.getId());
		}*/
		this.shoppingCartId = user.getShoppingCart().getId();
		this.restaurantId = user.getRestaurant().getId();
		this.ordersInNeedOfDeliveryIds = new ArrayList<String>();
		/*for (Order o : user.getOrdersInNeedOfDelivery()) {
			this.ordersInNeedOfDeliveryIds.add(o.getId());
		}*/
		this.collectedPoints = user.getCollectedPoints();
		this.buyerTypeId = user.getBuyerType().getId();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public ArrayList<String> getBoughtOrdersIds() {
		return boughtOrdersIds;
	}

	public void setBoughtOrdersIds(ArrayList<String> boughtOrdersIds) {
		this.boughtOrdersIds = boughtOrdersIds;
	}

	public long getShoppingCartId() {
		return shoppingCartId;
	}

	public void setShoppingCartId(long shoppingCartId) {
		this.shoppingCartId = shoppingCartId;
	}

	public long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}

	public ArrayList<String> getOrdersInNeedOfDeliveryIds() {
		return ordersInNeedOfDeliveryIds;
	}

	public void setOrdersInNeedOfDeliveryIds(ArrayList<String> ordersInNeedOfDeliveryIds) {
		this.ordersInNeedOfDeliveryIds = ordersInNeedOfDeliveryIds;
	}

	public double getCollectedPoints() {
		return collectedPoints;
	}

	public void setCollectedPoints(double collectedPoints) {
		this.collectedPoints = collectedPoints;
	}

	public long getBuyerTypeId() {
		return buyerTypeId;
	}

	public void setBuyerTypeId(long buyerTypeId) {
		this.buyerTypeId = buyerTypeId;
	}
}
