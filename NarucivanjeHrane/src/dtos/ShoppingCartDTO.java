package dtos;

import java.util.ArrayList;

import beans.Article;
import beans.ShoppingCart;

public class ShoppingCartDTO {
	private long id;
	private ArrayList<Long> articlesIds;
	private long buyerId;
	private double totalPrice;
	
	public ShoppingCartDTO() {}
	
	public ShoppingCartDTO(long id, ArrayList<Long> articlesIds, long buyerId, 
			double totalPrice) {
		this.id = id;
		this.articlesIds = articlesIds;
		this.buyerId = buyerId;
		this.totalPrice = totalPrice;
	}
	
	public ShoppingCartDTO(ShoppingCart shoppingCart) {
		this.id = shoppingCart.getId();
		this.articlesIds = new ArrayList<Long>();
		for (Article a : shoppingCart.getArticles()) {
			this.articlesIds.add(a.getId());
		}
		this.buyerId = shoppingCart.getBuyer().getId();
		this.totalPrice = shoppingCart.getTotalPrice();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public ArrayList<Long> getArticlesIds() {
		return articlesIds;
	}

	public void setArticlesIds(ArrayList<Long> articlesIds) {
		this.articlesIds = articlesIds;
	}

	public long getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(long buyerId) {
		this.buyerId = buyerId;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
}
