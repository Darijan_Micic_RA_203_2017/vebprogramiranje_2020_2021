package dtos;

import java.util.ArrayList;
import java.util.Date;

import beans.Article;
import beans.Order;
import beans.OrderStatus;

public class OrderDTO {
	private Long id;
	private ArrayList<Long> articlesIds;
	private long restaurantId;
	private Date dateAndTime;
	private double totalPrice;
	private long buyerId;
	private OrderStatus status;
	
	public OrderDTO() {}
	
	public OrderDTO(Long id, ArrayList<Long> articlesIds, long restaurantId, 
			Date dateAndTime, double totalPrice, long buyerId, OrderStatus status) {
		this.id = id;
		this.articlesIds = articlesIds;
		this.restaurantId = restaurantId;
		this.dateAndTime = dateAndTime;
		this.totalPrice = totalPrice;
		this.buyerId = buyerId;
		this.status = status;
	}
	
	public OrderDTO(Order order) {
		this.id = order.getId();
		this.articlesIds = new ArrayList<Long>();
		/*for (Article a : order.getArticles()) {
			this.articlesIds.add(a.getId());
		}*/
		this.restaurantId = order.getRestaurant().getId();
		this.dateAndTime = order.getDateAndTime();
		this.totalPrice = order.getTotalPrice();
		this.buyerId = order.getBuyer().getId();
		this.status = order.getStatus();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArrayList<Long> getArticlesIds() {
		return articlesIds;
	}

	public void setArticlesIds(ArrayList<Long> articlesIds) {
		this.articlesIds = articlesIds;
	}

	public long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}

	public Date getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public long getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(long buyerId) {
		this.buyerId = buyerId;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}
}
