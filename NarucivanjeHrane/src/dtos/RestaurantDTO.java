package dtos;

import java.util.ArrayList;

import beans.Article;
import beans.Restaurant;
import beans.RestaurantStatus;
import beans.RestaurantType;

public class RestaurantDTO {
	private long id;
	private String name;
	private RestaurantType type;
	private ArrayList<Long> articlesIds;
	private RestaurantStatus status;
	private long locationId;
	private String logoPath;
	
	public RestaurantDTO() {}
	
	public RestaurantDTO(long id, String name, RestaurantType type, 
			ArrayList<Long> articlesIds, RestaurantStatus status, long locationId, 
			String logoPath) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.articlesIds = articlesIds;
		this.status = status;
		this.locationId = locationId;
		this.logoPath = logoPath;
	}
	
	public RestaurantDTO(Restaurant restaurant) {
		this.id = restaurant.getId();
		this.name = restaurant.getName();
		this.type = restaurant.getType();
		this.articlesIds = new ArrayList<Long>();
		for (Article a : restaurant.getArticles()) {
			this.articlesIds.add(a.getId());
		}
		this.status = restaurant.getStatus();
		this.locationId = restaurant.getLocation().getId();
		this.logoPath = restaurant.getLogoPath();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RestaurantType getType() {
		return type;
	}

	public void setType(RestaurantType type) {
		this.type = type;
	}

	public ArrayList<Long> getArticlesIds() {
		return articlesIds;
	}

	public void setArticlesIds(ArrayList<Long> articlesIds) {
		this.articlesIds = articlesIds;
	}

	public RestaurantStatus getStatus() {
		return status;
	}

	public void setStatus(RestaurantStatus status) {
		this.status = status;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}
}
