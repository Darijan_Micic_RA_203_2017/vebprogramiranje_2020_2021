package services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Restaurant;
import beans.RestaurantStatus;
import beans.RestaurantType;
import beans.User;
import beans.UserRole;
import dao.RestaurantDAO;
import dao.UserDAO;

@Path("/restaurant")
public class RestaurantService {

	@Context
	ServletContext ctx;
	
	@PostConstruct
	public void init() {
		if(ctx.getAttribute("restaurantDAO")== null) {
			ctx.setAttribute("restaurantDAO", new RestaurantDAO(ctx.getRealPath("")));
		}
		if(ctx.getAttribute("userDAO") == null) {
			ctx.setAttribute("userDAO", new UserDAO(ctx.getRealPath("")));
		}
	}
	
	@GET
	@Path("/allRestaurants")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRestaurants(@Context HttpServletRequest request, User uu) {
		
		RestaurantDAO restaurants = (RestaurantDAO) ctx.getAttribute("restaurantDAO");
	
		List<Restaurant> allR = this.getAllR(restaurants);

		for(Restaurant r : restaurants.getRestaurants().values()) {
				allR.add(r);
			
		}

		return Response.ok(allR).build();
		
	}
	
	@GET
	@Path("/allRestaurantsManager")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRest(@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		RestaurantDAO restaurants = (RestaurantDAO) ctx.getAttribute("restaurantDAO");
	
		List<Restaurant> allR = this.getAllR(restaurants);
		List<Restaurant> rr = new ArrayList<Restaurant>();

		for(Restaurant r : restaurants.getRestaurants().values()) {
			if(r.getMaganer().getId() == loggedIn.getId())	{
				rr.add(r);
			}
			
		}

		return Response.ok(rr).build();
		
	}
	
	private List<Restaurant> getAllR(RestaurantDAO restaurants){
		ArrayList<Restaurant> usersArray = new ArrayList<Restaurant>();
		for(Restaurant u : restaurants.getRestaurants().values()) {
			usersArray.add(u);
		}
		return usersArray;
	}
	
	@POST
	@Path("/kreiraj")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response kreirajRestoran(Restaurant r, @Context HttpServletRequest request) {
		RestaurantDAO rDAO = (RestaurantDAO) ctx.getAttribute("restaurantDAO");
		User loggedIn = (User) request.getSession().getAttribute("user");
		System.out.println("Novi restoran je: " + r);
		Restaurant postoji = rDAO.findOneRestaurant(r.getId());
		
		if(postoji != null) {
			System.out.println("Taj restoran vec postoji!");
			return Response.status(400).build();
		}
		r.setAdmin(loggedIn);
		r.setStatus(RestaurantStatus.ACTIVE);
		rDAO.getRestaurants().put(r.getId(), r);
		ctx.setAttribute("restaurantDAO", rDAO);
		return Response.status(200).build();
	}
	
	@POST
	@Path("/search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResSearch(@Context HttpServletRequest request, String search) {
		User loggedIn = (User) request.getSession().getAttribute("user");
		RestaurantDAO restorani = (RestaurantDAO) ctx.getAttribute("restaurantDAO");
		List<Restaurant> aktivni = new ArrayList<Restaurant>();
		
		System.out.println("Search je: " + search);
		String[] searchArray = search.split(",");
		String naziv = ((searchArray[0]).split(":"))[1];
		String tip = ((searchArray[1]).split(":"))[1];
		String mesto = ((searchArray[2]).split(":"))[1];
		String ulica = ((searchArray[3]).split(":"))[1];
		String sortirajStatus = ((searchArray[4]).split(":"))[1];
			
			for(Restaurant a : restorani.getRestaurants().values()) {
				if(this.isNazivValid(a.getName(), naziv) 
						&& this.isTipValid(a.getType(), tip)
						&& this.isMestoValid(a.getLocation().getAddress().getPopulatedPlace(), mesto)
						&& this.isUlicaValid(a.getLocation().getAddress().getStreet(), ulica)) {	
									
					aktivni.add(a);			
				}
			}
		
		System.out.println("Pronadjeni restoran: " + aktivni);
		return Response.ok(aktivni).build();
	}
	
	
	private boolean isMestoValid(String mesto, String toSearch) {
		if(!toSearch.equals("")) {
			toSearch = toSearch.replace("\"", "");
			if(mesto.contains(toSearch)) {
				return true;
			}
			return false;
		}
		return true;
	}
	
	private boolean isNazivValid(String naziv, String toSearch) {
		if(!toSearch.equals("")) {
			toSearch = toSearch.replace("\"", "");
			if(naziv.contains(toSearch)) {
				return true;
			}
			return false;
		}
		return true;
	}
	
	private boolean isUlicaValid(String ulica, String toSearch) {
		if(!toSearch.equals("")) {
			toSearch = toSearch.replace("\"", "");
			if(ulica.contains(toSearch)) {
				return true;
			}
			return false;
		}
		return true;
	}

	private boolean isTipValid(RestaurantType tip, String toSearch) {
		if(!toSearch.equals("")) {
			toSearch = toSearch.replace("\"", "");
			if(tip.equals(toSearch)) {
				return true;
			}
			return false;
		}
		return true;
	}
	
	private boolean isStatusValid(RestaurantStatus status, String toSearch) {
		if(!toSearch.equals("")) {
			toSearch = toSearch.replace("\"", "");
			toSearch = toSearch.replace("}", "");
			System.out.println("statusValid: " + toSearch);
			if(toSearch.equals("")) {
				return true;
			}
			
			int statusSearch = Integer.parseInt(toSearch);
			if(status.equals(toSearch)) {
				return true;
			}
			return false;
		}
		return true;
	}
	
}
	


	

