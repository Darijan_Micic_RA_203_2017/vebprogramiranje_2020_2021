package services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Article;
import beans.Comment;
import beans.Order;
import beans.Restaurant;
import beans.RestaurantStatus;
import beans.User;
import dao.ArticleDAO;
import dao.CommentDAO;
import dao.OrderDAO;
import dao.RestaurantDAO;
import dao.UserDAO;

@Path("article")
public class ArticleService {
	
	@Context
	ServletContext ctx;
	
	@PostConstruct
	public void init() {
		if(ctx.getAttribute("articleDAO")==null) {
			ctx.setAttribute("articleDAO", new ArticleDAO(ctx.getRealPath("")));
		}
	}
	
	@GET
	@Path("/allArticles")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllArticles(@Context HttpServletRequest request, User uu) {
		
		ArticleDAO a = (ArticleDAO) ctx.getAttribute("articleDAO");
	
		List<Article> allA = this.getAllA(a);

		for(Article r : a.getArticles().values()) {
				allA.add(r);
			
		}

		return Response.ok(allA).build();
		
	}
	
	private List<Article> getAllA(ArticleDAO a){
		ArrayList<Article> usersArray = new ArrayList<Article>();
		for(Article u : a.getArticles().values()) {
			usersArray.add(u);
		}
		return usersArray;
	}
	
	@GET
	@Path("/allArticlesForOrder")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllArticlesOrder(@Context HttpServletRequest request, User uu) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		ArticleDAO a = (ArticleDAO) ctx.getAttribute("articleDAO");
		List<Article> art = new ArrayList<Article>();

		for(Article r : a.getArticles().values()) {
				if(r.getRestaurant().getId() == loggedIn.getRestaurant().getId()) {
					art.add(r);
				}
			
		}

		return Response.ok(art).build();
		
	}
	

	
	@GET
	@Path("/allArticlesForManager")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllArticlesM(@Context HttpServletRequest request, User uu) {
		User loggedIn = (User) request.getSession().getAttribute("user");
		
		ArticleDAO a = (ArticleDAO) ctx.getAttribute("articleDAO");
		List<Article> art = new ArrayList<Article>();

		for(Article r : a.getArticles().values()) {
				if(r.getRestaurant().getMaganer().getUsername().equals(loggedIn.getUsername())) {
					
					art.add(r);
				}

		}
		return Response.ok(art).build();
		
	}
	
	
	@POST
	@Path("/kreiraj")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response kreirajArtikl(Article r, @Context HttpServletRequest request) {
		ArticleDAO rDAO = (ArticleDAO) ctx.getAttribute("articleDAO");
		User loggedIn = (User) request.getSession().getAttribute("user");
		System.out.println("Novi artikl je: " + r);
		boolean postoji = rDAO.find(r.getName());
		
		if(postoji) {
			System.out.println("Taj artikl vec postoji!");
			return Response.status(400).build();
		}
		
		r.setRestaurant(loggedIn.getRestaurant());
		r.getRestaurant().setMaganer(loggedIn);
		rDAO.getArticles().put(r.getId(), r);
		ctx.setAttribute("articlesDAO", rDAO);
		return Response.status(200).build();
	}
	
	
	@POST
	@Path("/edit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response editApartman(Article a, @Context HttpServletRequest request) {
		ArticleDAO aDAO = (ArticleDAO) ctx.getAttribute("articleDAO");
		Article postoji = aDAO.findOneArt(a.getId());
		System.out.println("Artikl:" + a);
		
		if(postoji == null) {
			return Response.status(400).build();
		}
		postoji.setName(a.getName());
		postoji.setPrice(a.getPrice());
		postoji.setAmount(a.getAmount());
		postoji.setType(a.getType());
		postoji.setDescription(a.getDescription());

		System.out.println("Izmenjen artikl: " + postoji);
		
		aDAO.saveArt("");
		return Response.status(200).build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisiArtikl(@Context HttpServletRequest request, Article a) {
		
		ArticleDAO articles = (ArticleDAO) ctx.getAttribute("articleDAO");
		Article ap = articles.findOneArt(a.getId());
		
		ap.setObrisan(true); 		
		System.out.println("Obrisan je: " + ap);
		
		return Response.ok().build();
	}
}
