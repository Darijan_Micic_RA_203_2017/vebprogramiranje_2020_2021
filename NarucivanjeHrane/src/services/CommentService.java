package services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Article;
import beans.Comment;
import beans.CommentStatus;
import beans.Order;
import beans.OrderStatus;
import beans.User;
import dao.ArticleDAO;
import dao.CommentDAO;
import dao.OrderDAO;

@Path("/comment")
public class CommentService {
	
	@Context
	ServletContext ctx;
	
	@PostConstruct
	public void init() {
		if(ctx.getAttribute("commentDAO")==null) {
			ctx.setAttribute("commentDAO", new CommentDAO(ctx.getRealPath("")));
		}
	}
	
	@GET
	@Path("/allComments")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getComments (@Context HttpServletRequest request) {
		
		CommentDAO comments = (CommentDAO) ctx.getAttribute("commentDAO");
		List<Comment> allC = this.getAllC(comments);
		
		for(Comment o : comments.getComments().values()) {
			allC.add(o);
		}
		System.out.println(allC);
		
		return Response.ok(allC).build();
	}
	
	@GET
	@Path("/allAcceptB")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAcceptB (@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		CommentDAO comments = (CommentDAO) ctx.getAttribute("commentDAO");
		List<Comment> allC = this.getAllC(comments);
		
		for(Comment o : comments.getComments().values()) {
			if(o.getStatus().equals(CommentStatus.ACCEPT) && o.getBuyer() == loggedIn ) {
				
				allC.add(o);
			}
		}
		System.out.println(allC);
		
		return Response.ok(allC).build();
	}
	
	@GET
	@Path("/allAcceptM")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAcceptM (@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		CommentDAO comments = (CommentDAO) ctx.getAttribute("commentDAO");
		List<Comment> allC = this.getAllC(comments);
		
		for(Comment o : comments.getComments().values()) {
			if(o.getRestaurant().getMaganer() == loggedIn ) {
				
				allC.add(o);
			}
		}
		System.out.println(allC);
		
		return Response.ok(allC).build();
	}
	
	@GET
	@Path("/allAccept")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAccept (@Context HttpServletRequest request) {

		CommentDAO comments = (CommentDAO) ctx.getAttribute("commentDAO");
		List<Comment> allC = this.getAllC(comments);
		
		for(Comment o : comments.getComments().values()) {
			if(o.getStatus().equals(CommentStatus.ACCEPT)  ) {
				
				allC.add(o);
			}
		}
		System.out.println(allC);
		
		return Response.ok(allC).build();
	}
	
	@POST
	@Path("/dodaj")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response kreirajKomentar(Comment r, @Context HttpServletRequest request) {
		CommentDAO rDAO = (CommentDAO) ctx.getAttribute("commentDAO");
		User loggedIn = (User) request.getSession().getAttribute("user");
		System.out.println("Novi komentar je: " + r);
		
		
		r.setBuyer(loggedIn);
		r.setStatus(CommentStatus.IN_PROCESS);
		rDAO.getComments().put(r.getId(), r);
		ctx.setAttribute("commentDAO", rDAO);
		return Response.status(200).build();
	}
	
	@POST
	@Path("/odobri")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response statusOdobri(@Context HttpServletRequest request, Comment a) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		CommentDAO comment = (CommentDAO) ctx.getAttribute("commentDAO");
		Comment co = comment.findOne(a.getId());
		
		if(co.getRestaurant().getMaganer()==loggedIn && co.getStatus().equals(CommentStatus.IN_PROCESS)) {
			co.setStatus(CommentStatus.ACCEPT); 
		}
		comment.saveComment("");
		
		return Response.ok().build();
	}

	@POST
	@Path("/odobren")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response statusOdobren(@Context HttpServletRequest request, Comment a) {
		
		CommentDAO comment = (CommentDAO) ctx.getAttribute("commentDAO");
		Comment co = comment.findOne(a.getId());
		
		co.setStatus(CommentStatus.ACCEPT); 
		comment.saveComment("");
		
		return Response.ok().build();
	}
	

	@POST
	@Path("/odbijen")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response statusOdbijen(@Context HttpServletRequest request, Comment a) {
		
		CommentDAO comment = (CommentDAO) ctx.getAttribute("commentDAO");
		Comment co = comment.findOne(a.getId());
		System.out.println(co);
		
		co.setStatus(CommentStatus.DECLINE); 
		comment.saveComment("");
		
		return Response.ok().build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisiKomentar(@Context HttpServletRequest request, Comment a) {
		
		CommentDAO comments = (CommentDAO) ctx.getAttribute("commentDAO");
		Comment ap = comments.findOne(a.getId());
		
		ap.setObrisan(true); 		
		System.out.println("Obrisan je: " + ap);
		
		return Response.ok().build();
	}
	
	private List<Comment> getAllC(CommentDAO comments){
		ArrayList<Comment> usersArray = new ArrayList<Comment>();
		for(Comment u : comments.getComments().values()) {
			usersArray.add(u);
		}
		return usersArray;
	}

}
