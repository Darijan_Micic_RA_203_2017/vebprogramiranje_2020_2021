package services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.RestaurantDAO;
import dao.UserDAO;
import beans.Restaurant;
import beans.User;
import beans.UserRole;

@Path("/user")
public class UserService {

	@Context
	ServletContext ctx;
	
	@PostConstruct
	public void init() {
		if(ctx.getAttribute("userDAO")==null) {
			ctx.setAttribute("userDAO", new UserDAO(ctx.getRealPath("")));
		}
	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");
		
		return Response.ok(loggedIn).build();
	}
	
	@POST
	@Path("/edit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response userEdit(@Context HttpServletRequest request, User u) {
		User loggedIn = (User) request.getSession().getAttribute("user");
		UserDAO users = (UserDAO) ctx.getAttribute("userDAO");
	
		System.out.println("Podaci su: " + u);
		User user = users.findUser(loggedIn.getId());
		user.setFirstName(u.getFirstName());
		user.setLastName(u.getLastName());
		user.setPassword(u.getPassword());
		user.setGender(u.getGender());
		users.saveUsers("");
		
		
		System.out.println("Novi user je: " + loggedIn);
		return Response.ok(loggedIn).build();
	}
	
	@GET
	@Path("/allUsers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsersKorisnik(@Context HttpServletRequest request) {
		UserDAO users = (UserDAO) ctx.getAttribute("userDAO");
		
		List<User> usersArray = this.getAllUsers(users);
		
		return Response.ok(usersArray).build();
	}
	
	private List<User> getAllUsers(UserDAO users){
		ArrayList<User> usersArray = new ArrayList<User>();
		for(User u : users.getUsers().values()) {
			usersArray.add(u);
		}
		
		return usersArray;
	}
	
	@POST
	@Path("/admin/users/search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsersKorisnikSearch(@Context HttpServletRequest request, String search) {
		UserDAO users = (UserDAO) ctx.getAttribute("userDAO");
		
		String[] searchArray = search.split(",");
		String username = ((searchArray[0]).split(":"))[1];
		String ime = ((searchArray[1]).split(":"))[1];
		String prezime = ((searchArray[2]).split(":"))[1];
		
		List<
		User> usersArray = this.getAllUsers(users);
		
		List<User> ok = new ArrayList<User>();
		for(User u : usersArray) {
			if(this.isUsernameValid(u.getUsername(), username)
					&& this.isFirstNameValid(u.getFirstName(), ime)
					&& this.isLastNameValid(u.getLastName(), prezime)) {
				System.out.println(u);
				ok.add(u);
			}
		}
		System.out.println("Search "+ok);
		
		return Response.ok(ok).build();
	}
	
	
	/*@GET
	@Path("/restForManager")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRes(@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		RestaurantDAO restaurants = (RestaurantDAO) ctx.getAttribute("restaurantDAO");
		List<Restaurant> r = new ArrayList<Restaurant>();
		
		System.out.println(restaurants);
		
		for(Restaurant rest : restaurants.getRestaurants().values()) {
			if(rest.getMaganer().getId() == loggedIn.getId() ) { 
				r.add(rest);
			}
		}
		
		return Response.ok(r).build();
	}
	*/
	@GET
	@Path("/allRes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getallRes(@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");
		RestaurantDAO ress = (RestaurantDAO) ctx.getAttribute("restaurantDAO");

		
		List<Restaurant> usersArray = this.getAllRess(ress);
		
		return Response.ok(usersArray).build();
	}
	
	
	private List<Restaurant> getAllRess(RestaurantDAO r){
		ArrayList<Restaurant> usersArray = new ArrayList<Restaurant>();
		for(Restaurant u : r.getRestaurants().values()) {
			usersArray.add(u);
		}
		
		return usersArray;
	}
	
	private boolean isUsernameValid(String username, String toSearch) {
		if(!toSearch.equals("")) {
			toSearch = toSearch.replace("\"", "");
			if(username.contains(toSearch)) {
				return true;
			}			return false;
		}
		return true;
	}
	
	private boolean isFirstNameValid(String ime, String toSearch) {
		if(!toSearch.equals("")) {
			toSearch = toSearch.replace("\"", "");
			if(ime.contains(toSearch)) {
				return true;
			}
			return false;
		}
		return true;
	}
	
	private boolean isLastNameValid(String prezime, String toSearch) {
		if(!toSearch.equals("")) {
			toSearch = toSearch.replace("\"", "");
			if(prezime.contains(toSearch)) {
				return true;
			}
			return false;
		}
		return true;
	}
	
	
	@GET
	@Path("/allManagers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsersManagers(@Context HttpServletRequest request) {
		UserDAO users = (UserDAO) ctx.getAttribute("userDAO");
		
		List<User> menagers = new ArrayList<User>();
		List<User> usersArray = this.getAllUsers(users);
		for(User u : users.getUsers().values()) {
			if(u.getRole() == UserRole.MANAGER) {
				menagers.add(u);
			}
		}
		
		return Response.ok(menagers).build();
	}
	

	

}