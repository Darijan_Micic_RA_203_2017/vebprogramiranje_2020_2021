package services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Article;
import beans.Order;
import beans.OrderStatus;
import beans.User;
import beans.UserRole;
import dao.OrderDAO;


@Path("/order")
public class OrderService {

	
	@Context
	ServletContext ctx;
	
	@PostConstruct
	public void init() {
		if(ctx.getAttribute("orderDao")==null) {
			ctx.setAttribute("orderDAO", new OrderDAO(ctx.getRealPath("")));
		}
	}
	
	
	@GET
	@Path("/allOrders")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrders (@Context HttpServletRequest request) {
		
		OrderDAO orders = (OrderDAO) ctx.getAttribute("orderDAO");
		List<Order> allO = this.getAllO(orders);
		
		for(Order o : orders.getOrders().values()) {
			allO.add(o);
		}
		System.out.println(allO);
		
		return Response.ok(allO).build();
	}
	
	

	@GET
	@Path("/allOrdersManager")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrdersManager (@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		OrderDAO orders = (OrderDAO) ctx.getAttribute("orderDAO");
		List<Order> allO = this.getAllO(orders);
		List<Order> ord = new ArrayList<Order>();
		
		for(Order o : orders.getOrders().values()) {
			if(o.getRestaurant().getMaganer().getId() == loggedIn.getId()) {
				ord.add(o);
			}
		}
		
		
		return Response.ok(ord).build();
	}
	
	@GET
	@Path("/allOrdersB")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrdersB (@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		OrderDAO orders = (OrderDAO) ctx.getAttribute("orderDAO");
		List<Order> allO = this.getAllO(orders);
		List<Order> ord = new ArrayList<Order>();
		
		for(Order o : orders.getOrders().values()) {
			if(o.getBuyer().getId() == loggedIn.getId()) {
				ord.add(o);
			}
		}
		System.out.println(ord);
		
		
		return Response.ok(ord).build();
	}
	
	
	//prikaz nedostavljenih porudzbina
	//moja logika: ako je nedostavljena - onda je sve sem dosravljena haha
	@GET
	@Path("/allOrdersBNedostavljene")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrdersBNed (@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		OrderDAO orders = (OrderDAO) ctx.getAttribute("orderDAO");
		List<Order> allO = this.getAllO(orders);
		List<Order> ord = new ArrayList<Order>();
		
		for(Order o : orders.getOrders().values()) {
			if(o.getBuyer().getId() == loggedIn.getId() && o.getStatus() != OrderStatus.DELIVERED) {
				ord.add(o);
			}
		}
		
		
		return Response.ok(ord).build();
	}
	
	

	
	@GET
	@Path("/allOrdersD")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrdersD (@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		OrderDAO orders = (OrderDAO) ctx.getAttribute("orderDAO");
		List<Order> allO = this.getAllO(orders);
		List<Order> ord = new ArrayList<Order>();
		
		for(Order o : orders.getOrders().values()) {
			//if(o.getDeliverer().equals(loggedIn)) {
			if(o.getStatus().equals(OrderStatus.IN_TRANSPORT)) {
				ord.add(o);
			}
		}
		System.out.println(ord);
		
		
		return Response.ok(ord).build();
	}
	
	
	@GET
	@Path("/allOrdersDNedostavljene")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrdersDNed (@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		OrderDAO orders = (OrderDAO) ctx.getAttribute("orderDAO");
		List<Order> allO = this.getAllO(orders);
		List<Order> ord = new ArrayList<Order>();
		
		for(Order o : orders.getOrders().values()) {
				if( o.getStatus() != OrderStatus.DELIVERED) {
					ord.add(o);
				}
			
		}
		
		
		return Response.ok(ord).build();
	}
	
	
	
	
	
	@GET
	@Path("/allOrdersDCeka")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrdersDCeka (@Context HttpServletRequest request) {
		User loggedIn = (User) request.getSession().getAttribute("user");

		OrderDAO orders = (OrderDAO) ctx.getAttribute("orderDAO");
		List<Order> allO = this.getAllO(orders);
		List<Order> ord = new ArrayList<Order>();
		
		for(Order o : orders.getOrders().values()) {
			if(o.getStatus() == OrderStatus.WAITING_FOR_DELIVERER) {
				ord.add(o);
			}
		}
		
		
		return Response.ok(ord).build();
	}
	
	private List<Order> getAllO(OrderDAO orders){
		ArrayList<Order> usersArray = new ArrayList<Order>();
		for(Order u : orders.getOrders().values()) {
			usersArray.add(u);
		}
		return usersArray;
	}
	
	@POST
	@Path("/status/odustao")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response statusOdustao(@Context HttpServletRequest request, Order a) {
		
		OrderDAO order = (OrderDAO) ctx.getAttribute("orderDAO");
		Order or = order.findOneOrder(a.getId());
		
		or.setStatus(OrderStatus.CANCELLED); 
		or.getBuyer().setCollectedPoints(or.getTotalPrice()/1000*133*4);
		
		order.saveOrder("");
		return Response.ok().build();
	}
	

	@POST
	@Path("/status/pripremljena")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response statusPripremljena(@Context HttpServletRequest request, Order a) {
		
		OrderDAO order = (OrderDAO) ctx.getAttribute("orderDAO");
		Order or = order.findOneOrder(a.getId());
		
		or.setStatus(OrderStatus.WAITING_FOR_DELIVERER); 
		order.saveOrder("");
		
		return Response.ok().build();
	}
	
	@POST
	@Path("/status/dostavljena")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response statusDostavljena(@Context HttpServletRequest request, Order a) {
		
		OrderDAO order = (OrderDAO) ctx.getAttribute("orderDAO");
		Order or = order.findOneOrder(a.getId());
		
		or.setStatus(OrderStatus.DELIVERED); 
		order.saveOrder("");
		
		return Response.ok().build();
	}
	
	@POST
	@Path("/status/zatrazi")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response statusZatrazi(@Context HttpServletRequest request, Order a) {
		
		OrderDAO order = (OrderDAO) ctx.getAttribute("orderDAO");
		Order or = order.findOneOrder(a.getId());
		
		or.setReq(1); 
		order.saveOrder("");
		
		return Response.ok().build();
	}
}
